import os

from can_trace_parser import CANTraceParser
from can_message_aggregrator import CANMessageAggregator
import json


def get_all_ext_in_folder(folder=os.path.dirname(__file__) + '/trc', extensions =['trc', 'txt']):
    trc_files = []
    files_in_dir = os.listdir(folder)
    for file_in_dir in files_in_dir:
        if file_in_dir.split('.')[-1] in extensions:
            if len(folder) > 0:
                trc_files.append(folder + '/' + file_in_dir)
            else:
                trc_files.append(file_in_dir)

    return trc_files


def process_trc_to_agg(parser, aggregator, trc_file, agg_file):
    messages = aggregator.aggregate_messages(parser.parse_trace(trc_file))
    with open(agg_file, 'w') as f:
        for message in messages:
            f.write(str(message) + '\n')


def needs_remaking(last_edited, trc_file):
    if trc_file in last_edited:
        if last_edited[trc_file] == os.path.getmtime(trc_file):
            return False
    return True

def calculate_agg_file(trc_file, agg_folder):
    return '{}/{}.agg'.format(agg_folder, trc_file.split('/')[-1].split('.')[0])

if __name__ == '__main__':
    trc_folder = os.path.dirname(__file__) + '/trc'
    agg_folder = os.path.dirname(__file__) + '/agg'
    last_edited_json = os.path.dirname(__file__) + '/last_edited.json'

    TEST = False

    try:
        with open(last_edited_json, 'r') as fp:
            last_edited = json.load(fp)
    except:
        last_edited = {}

    ids=[]
    if len(ids) == 0:
        parser = CANTraceParser(ids=ids, include=False)
    else:
        parser = CANTraceParser(ids=ids)

    aggregator = CANMessageAggregator()
    trc_files = get_all_ext_in_folder(folder=trc_folder)

    for trc_file in trc_files:
        agg_file = calculate_agg_file(trc_file, agg_folder)
        if TEST:
            process_trc_to_agg(parser, aggregator, trc_file, agg_file)
            last_edited[trc_file] = os.path.getmtime(trc_file)
        if needs_remaking(last_edited, trc_file):
            process_trc_to_agg(parser, aggregator, trc_file, agg_file)
            last_edited[trc_file] = os.path.getmtime(trc_file)
            # try:
            #     process_trc_to_agg(parser, aggregator, trc_file, agg_file)
            #     last_edited[trc_file] = os.path.getmtime(trc_file)
            # except:
            #     print("Something went wrong during processing file: \t%s" % trc_file)

    with open(last_edited_json, 'w') as fp:
        json.dump(last_edited, fp)
