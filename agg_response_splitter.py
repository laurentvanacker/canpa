import os
import json

from batch_trc_processing import get_all_ext_in_folder


def split_responses(agg_file):
    responses = {}
    folder = agg_file[:-4]
    if not os.path.exists(folder):
        os.mkdir(folder)
    with open(agg_file, 'r') as f:
        for line in f.readlines():
            message_parts = line.strip().split(',')
            timestamp, message_id, message_type = message_parts[
                0], message_parts[1], message_parts[2]
            if message_type == 'SF' or message_type == 'AM':
                message_data = message_parts[-1]
                response_id = message_data[:4]
                response_name = 'response_{}_{}'.format(
                    message_id, response_id)
                if response_name in responses:
                    responses[response_name].append(
                        '{},{}'.format(timestamp, message_data))
                else:
                    responses[response_name] = [
                        '{},{}'.format(timestamp, message_data)]
    with open(folder + '/all.json', 'w') as fp:
        json.dump(responses, fp)

    for response_type, collected_messages in responses.items():
        with open(folder + '/' + response_type, 'w') as f:
            for message in collected_messages:
                f.write(message + '\n')


if __name__ == '__main__':
    agg_folder = os.path.dirname(__file__) + '/agg'
    agg_files = get_all_ext_in_folder(folder=agg_folder, extensions='agg')
    print(agg_files)
    for agg_file in agg_files:
        split_responses(agg_file)
