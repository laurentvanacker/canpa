#!/usr/bin/env python

# TODO: clear up with what belongs where and refactor.
# as IDs make it pretty hard to know what to and what not to

from can_message_parser import CANMessageParser


class CANTraceParser(object):

    def __init__(self, include=True, ids=[], standard=None):
        self.message_parser = CANMessageParser(standard)
        self.include = include
        self.ids = ids

    def trim_can_data(self, can_data):
        delete_indexes = []
        for i in range(len(can_data)):
            if '\n' in can_data[i]:
                can_data[i] = can_data[i].strip()
            if can_data[i] == '':
                delete_indexes.append(i)
        for i in delete_indexes[-1::-1]:
            del can_data[i]
        return can_data

    def parse_trace(self, filename):
        """returns a list of all CAN messages in the trace"""
        messages = []
        with open(filename, 'r') as can_data_file:
            lines = can_data_file.readlines()
            for line in lines:
                can_data = line.split(' ')
                self.trim_can_data(can_data)
                # check whether line is a valid measurement
                try:
                    message_timestamp, message_id, message_dlc, message_data = float(
                        can_data[0].replace(',', '.')), can_data[1], int(can_data[2]), can_data[3:]
                    if self.valid_id(message_id):
                        messages.append(self.make_message(
                            message_timestamp, message_id, message_dlc, message_data))
                except:
                    continue
        return messages

    def valid_id(self, message_id):
        if self.include is True and message_id in self.ids:
            return True
        elif self.include is False and message_id not in self.ids:
            return True
        return False

    def make_message(self, *args):
        message = self.message_parser.parse_message(*args)
        return message

    def add_ids(self, ids):
        for id in ids:
            if id not in self.ids:
                self.ids.append(id)

if __name__ == '__main__':
    import os
    parser = CANTraceParser(ids=[])
    # messages = parser.parse_trace(os.path.dirname(__file__) + '/Battery_CAN.txt')
    messages = parser.parse_trace(os.path.dirname(__file__)+'/trc/test.trc')
    # for message in messages:
    #     print(message)
    # print(messages)
