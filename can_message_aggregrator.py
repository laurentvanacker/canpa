# TODO: Aggregate the SF and CF frames successfully.

from can_messages_iso_j15756 import CANMessage

DATA_BYTES_PER_CONSECUTIVE_FRAME = 7
MESSAGE_BYTES_PER_CONSECUTIVE_FRAME = 8


class AggregatedMessage(CANMessage):
    type = 'AM'

    def __init__(self, first_frame_message):
        self.first_frame_message = first_frame_message
        self.consecutive_frame_messages = {}
        self.message_timestamp = first_frame_message.message_timestamp
        self.message_dlc = first_frame_message.message_dlc
        self.message_id = first_frame_message.message_id
        self.length = first_frame_message.length

    def add_consecutive_frame(self, consecutive_frame):
        if not consecutive_frame.sequence_number in self.consecutive_frame_messages:
            self.consecutive_frame_messages[
                consecutive_frame.sequence_number] = consecutive_frame
        else:
            print("Duplicate CF, no new FF received then?")

    @property
    def data_bytes(self):
        data_bytes = self.first_frame_message.data_bytes
        for i in range(len(self.consecutive_frame_messages)):
            if i + 1 in self.consecutive_frame_messages:
                data_bytes += self.consecutive_frame_messages[i + 1].data_bytes
            else:
                # FM means frame missing
                data_bytes += ['FM'] * DATA_BYTES_PER_CONSECUTIVE_FRAME
        if len(data_bytes) > self.length and not 'MI' in data_bytes:
            data_bytes = data_bytes[:self.length]
        while len(data_bytes) < self.length:
            # MI means message incomplete (padding)
            data_bytes += ['MI']
        return data_bytes

    @property
    def message_data(self):
        message_data = self.first_frame_message.message_data
        for i in range(len(self.consecutive_frame_messages)):
            if i + 1 in self.consecutive_frame_messages:
                message_data += self.consecutive_frame_messages[i + 1].message_data
            else:
                message_data += ['MI'] * MESSAGE_BYTES_PER_CONSECUTIVE_FRAME
        return message_data

    @property
    def additional_print_data(self):
        return ',{},{}'.format(self.length, ''.join(self.data_bytes))


class CANMessageAggregator(object):

    def __init__(self):
        pass

    def aggregate_messages(self, can_messages):
        collected_messages = []
        currently_aggregating_messages = {}
        for can_message in can_messages:
            if can_message.type == 'SF' or can_message.type == 'FCF':
                collected_messages.append(can_message)
            elif can_message.type == 'FF':
                if can_message.message_id in currently_aggregating_messages:
                    collected_messages.append(
                        currently_aggregating_messages[can_message.message_id])
                currently_aggregating_messages[
                    can_message.message_id] = AggregatedMessage(can_message)
            elif can_message.type == 'CF':
                if can_message.message_id in currently_aggregating_messages:
                    currently_aggregating_messages[can_message.message_id].add_consecutive_frame(can_message)
                else:
                    print("Dropping %s" % can_message)

        for message_id, message in currently_aggregating_messages.items():
            collected_messages.append(message)

        return collected_messages


if __name__ == '__main__':
    from can_trace_parser import CANTraceParser
    parser = CANTraceParser(ids=[])

    aggregator = CANMessageAggregator()
    messages = aggregator.aggregate_messages(parser.parse_trace('derp.trc'))

    with open('collected_derp.txt', 'w') as f:
        for message in messages:
            f.write(str(message) + '\n')
