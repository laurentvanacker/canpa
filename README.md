## CANPA

CANPA stands for **CAN** **P**acket **A**ggregator

It is meant to comply with the [ISO-TP](https://en.wikipedia.org/wiki/ISO_15765-2) standard that is used with CAN packets.

## Instructions

### For exporting the trc files to agg (aggregated) files

- Put the .trc trace files in the trc folder
- Fill in the ids array in batch_trc_processing.py's ifmain structure below. For example, if you want to get the packets of ID 12E and A56, replace ids=[] with ids=['12E', 'A56']
- Run batch_trc_processing.py

### For splitting the agg files per response type and ID

- Run agg_response_splitter.py

### Agg files?
Aggregated files, ending with .agg, are files wherein the packets are deserialized in full complete packets.
You can split these down even further for easier analysis of relevant packets.
For convenience, a JSON file is also created.

## NOTE

CANPA keeps a record of which files were exported and whether they were edited in
the meantime, which causes file to only be exported once. Thus, you can add files
and run the batch exporting again, and only the new files will be exported.

_You can remove this behaviour by deleting the last_edited.json file._

Enjoy!
