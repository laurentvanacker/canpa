#!/usr/bin/env python
"""
can_packets.py - contains all CAN packet object definitions

Not sure if this works for other than the DLC
"""


class CANMessage(object):
    standard = 'ISO-J15756'
    type = 'undefined'

    def __init__(self, message_timestamp, message_dlc, message_id, message_data):
        self.message_timestamp = message_timestamp
        self.message_dlc = message_dlc
        self.message_id = message_id
        self.message_data = message_data
        self.make_packet()

    def make_packet(self):
        print("Use a defined packet, nothing happens now.")

    def __str__(self):
        return '{},{},{}'.format(self.message_timestamp, self.message_id, self.type) + self.additional_print_data

    @property
    def additional_print_data(self):
        return ''


class SingleFrame(CANMessage):
    type = 'SF'

    def make_packet(self):
        self.length = int(self.message_data[0], 16) & 0xF
        self.data_bytes = self.message_data[1:self.length + 1]

    @property
    def additional_print_data(self):
        return ',{},{}'.format(self.length, ''.join(self.data_bytes))


class FirstFrame(CANMessage):
    type = 'FF'

    def make_packet(self):
        self.length = (
            (int(self.message_data[0], 16) & 0xF) << 8) + int(self.message_data[1], 16)
        self.data_bytes = self.message_data[2:]

    @property
    def additional_print_data(self):
        return ',{},{}'.format(self.length, ''.join(self.data_bytes))


class ConsecutiveFrame(CANMessage):
    type = 'CF'

    def make_packet(self):
        self.sequence_number = int(self.message_data[0], 16) & 0xF
        self.data_bytes = self.message_data[1:]

    @property
    def additional_print_data(self):
        return ',{},{}'.format(self.sequence_number, ''.join(self.data_bytes))


class FlowControlFrame(CANMessage):
    type = 'FCF'

    def make_packet(self):
        self.flow_status = int(self.message_data[0], 16) & 0xF

    def __str__(self):
        return super().__str__() + ',{}'.format(self.flow_status)

    @property
    def additional_print_data(self):
        return ',{}'.format(self.flow_status)
