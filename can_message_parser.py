#!/usr/bin/env python

"""
can_message_parser.py -- parses CAN data into a message
"""

class CANMessageParser(object):

    def __init__(self, standard=None):
        if standard is None:
            standard = 'ISO-J15756'
        self.standard = standard
        self.get_standard_parse_message()


    def parse_message_ISO_J15756(self, message_timestamp, message_id, message_dlc, message_data):
        from can_messages_iso_j15756 import SingleFrame, FirstFrame, ConsecutiveFrame, FlowControlFrame

        packet_identifier = int(message_data[0], 16) >> 4
        if packet_identifier == 0:
            return SingleFrame(message_timestamp, message_dlc, message_id, message_data)
        elif packet_identifier == 1:
            return FirstFrame(message_timestamp, message_dlc, message_id, message_data)
        elif packet_identifier == 2:
            return ConsecutiveFrame(message_timestamp, message_dlc, message_id, message_data)
        elif packet_identifier == 3:
            return FlowControlFrame(message_timestamp, message_dlc, message_id, message_data)

    def get_standard_parse_message(self):
        standards = {
            'ISO-J15756': self.parse_message_ISO_J15756
        }
        self.parse_message = standards[self.standard]
